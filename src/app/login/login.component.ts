import { Component, OnInit,Inject, SimpleChanges } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../service/personne/login.service';
import { MatSnackBar } from '@angular/material/snack-bar';
export interface Token {
  token: string;
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private credential = {'username':'', 'password':''}; 
	private loggedIn = false; 

  addressForm = this.fb.group({
    username: [null, Validators.required],
    password: [null, Validators.required],
  });

  constructor(private fb: FormBuilder,private loginService: LoginService,private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.loginService.checkSession().subscribe(
      res =>   {
        this.loggedIn = true;
          
        console.log(this.loggedIn);
      },
      error => {
        console.log("===================================== false:"+error);

        this.loggedIn = false;
        
      }
    )
      
  }

  onSubmit(){
  

  }


  loginUser(event){
    this.credential.username =this.addressForm.get("username").value;
    this.credential.password =this.addressForm.get("password").value;
    if(this.credential.username!=null && this.credential.password!=null && this.credential.username!='' && this.credential.password!=''){
    console.log(this.credential);
    this.loginService.sendCredential(this.credential.username, this.credential.password).subscribe(
  		res => {
        //console.log(res.token);

                localStorage.setItem("xAuthToken", res.token);
                console.log(localStorage);
                this.loggedIn = true;
                
                location.reload();
        
  		}, 
  		error => {
        this._snackBar.open("Veuillez vérifier votre nom d'utilisateur/mot de passe","Fermer", {
          duration: 5000,
        });
  			console.log(error);
  		}

  		)
  }
  }

}
