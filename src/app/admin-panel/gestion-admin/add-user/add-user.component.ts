import { Component, Inject, SimpleChanges, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdresseService } from 'src/app/service/adresse/adresse.service';
import { Adresse } from 'src/app/models/adresseModel';
import { Personne } from 'src/app/models/personneModel';
import { PersonneService } from 'src/app/service/personne/personne.service';

  export interface DialogData {
    admin:Boolean;
    personne:Personne;
  }
@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css'],
 
})
export class AddUserComponent implements OnInit {
  states = [
    {name: 'Ariana', abbreviation: 'Ariana'},
    {name: 'Béja', abbreviation: 'Béja'},
    {name: 'Ben Arous', abbreviation: 'Ben Arous'},
    {name: 'Bizerte', abbreviation: 'Bizerte'},
    {name: 'Gabès', abbreviation: 'Gabès'},
    {name: 'Gafsa', abbreviation: 'Gafsa'},
    {name: 'Jendouba', abbreviation: 'Jendouba'},
    {name: 'Kairouan', abbreviation: 'Kairouan'},
    {name: 'Kasserine', abbreviation: 'Kasserine'},
    {name: 'Kebili', abbreviation: 'Kebili'},
    {name: 'Kef', abbreviation: 'Kef'},
    {name: 'Mahdia', abbreviation: 'Mahdia'},
    {name: 'Manouba', abbreviation: 'Manouba'},
    {name: 'Medenine', abbreviation: 'Medenine'},
    {name: 'Monastir', abbreviation: 'Monastir'},
    {name: 'Nabeul', abbreviation: 'Nabeul'},
    {name: 'Sfax', abbreviation: 'Sfax'},
    {name: 'Sidi Bouzid', abbreviation: 'Sidi Bouzid'},
    {name: 'Siliana', abbreviation: 'Siliana'},
    {name: 'Sousse', abbreviation: 'Sousse'},
    {name: 'Tataouine', abbreviation: 'Tataouine'},
    {name: 'Tozeur', abbreviation: 'Tozeur'},
    {name: 'Tunis', abbreviation: 'Tunis'},
    {name: 'Zaghouan', abbreviation: 'Zaghouan'}
  ];
    date:String;
ngOnChanges(changes: SimpleChanges): void {
 
  this.date = new Date().toISOString().slice(0, 16);

}
  addressForm = this.fb.group({
    adresse: [null, Validators.required],
    Ville: [null, Validators.required],
    Gouvernorat: [null, Validators.required],
    Pays: [null, Validators.required],
    postalCode: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(5)])],
   
    cin:[null, Validators.required],
    prenom:[null, Validators.required],
    nom:[null, Validators.required],
    DateNaiss:new FormControl((new Date()).toISOString()),
    Emailuser:new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
  
    username:[null, Validators.required],
    password:new FormControl()

  });




  hasUnitNumber = false;

 


  constructor(private fb: FormBuilder,
    private adresseService : AdresseService,
    private personneService: PersonneService,
    public dialogRef: MatDialogRef<AddUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    }

  ngOnInit(): void {
    if (this.data.personne!=null){
      console.log(this.data.personne.username);
    this.addressForm.get('cin').setValue(this.data.personne.cin);  
    this.addressForm.get('prenom').setValue(this.data.personne.prenom);  
    this.addressForm.get('nom').setValue(this.data.personne.nom);  
    this.addressForm.get('Emailuser').setValue(this.data.personne.email);  
    this.addressForm.get('DateNaiss').setValue(new Date(this.data.personne.dateNaiss)); 
    this.addressForm.get('username').setValue(this.data.personne.username);

      
    this.adresseService.getOne(this.data.personne.idAdr).subscribe(res =>{
      
      this.addressForm.get('adresse').setValue(res.adresse); 
      this.addressForm.get('Ville').setValue(res.ville); 
      this.addressForm.get('Gouvernorat').setValue(res.gouvernorat); 
      this.addressForm.get('Pays').setValue(res.pays); 
      this.addressForm.get('postalCode').setValue(res.codePostal); 
    })  
  } 
  }
  onSubmit() {

    
    let personne = new Personne();
    let adresse = new Adresse();
   
    
if (this.data.personne!=null){
  adresse.idAdr=this.data.personne.idAdr;
}
    personne.cin          =this.addressForm.get('cin').value;
    personne.prenom       =this.addressForm.get('prenom').value;
    personne.nom          =this.addressForm.get('nom').value
    personne.dateNaiss    =this.addressForm.get('DateNaiss').value;
    personne.email        =this.addressForm.get('Emailuser').value;
    personne.username     =this.addressForm.get('username').value;
    personne.status       =true;
    if(this.data.admin){
      personne.superior= personne.username;
    }else{
      personne.superior=localStorage.getItem("user");
    }


if(this.addressForm.get('password').value==null || this.addressForm.get('password').value ==''){
  personne.password = this.data.personne.password;
}else{
  personne.password = this.addressForm.get('password').value
}


    adresse.adresse =this.addressForm.get("adresse").value;
    adresse.ville =this.addressForm.get("Ville").value;
    adresse.gouvernorat =this.addressForm.get("Gouvernorat").value;
    adresse.pays =this.addressForm.get("Pays").value;
    adresse.codePostal =this.addressForm.get("postalCode").value;

if(personne.prenom!=null&&personne.nom!=null&&personne.dateNaiss!=null&&personne.email!=null&&personne.username!=null&&personne.cin!=null
   && adresse.adresse!=null&&adresse.ville!=null&&adresse.gouvernorat!=null&&adresse.pays!=null&&adresse.codePostal!=null){
    
    
    
    this.adresseService.AddOne(adresse).subscribe(
      data => {
        personne.idAdr     = data.idAdr;
      },
      err =>{},
      ()=>{

          this.personneService.AddOne(personne).subscribe(data=>{
            console.log(data);
          },
          err=>{},
          ()=>{
            this.dialogRef.close(true);
          });


      }
    );


   } 
  }

  
  onNoClick(): void {
    this.dialogRef.close();
  }
}
