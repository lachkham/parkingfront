export class Parking{
    idParking   : number;
    libelle     : string;
    longitude   : number;
    latitude    : number;
    idAdr       : number;
    typeId      : number;
    nbPlace     : number;
    distance    : number;
}