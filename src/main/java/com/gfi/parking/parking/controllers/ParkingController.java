package com.gfi.parking.parking.controllers;


import com.gfi.parking.parking.DTO.ParkingDTO;
import com.gfi.parking.parking.service.ParkingService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/v1/parking")
@CrossOrigin(origins = "*")

public class ParkingController {

    private final ParkingService parkingService;

    public ParkingController(ParkingService parkingService) {
        this.parkingService = parkingService;
    }

    @GetMapping("/")
    public ResponseEntity findAll(Pageable pageable){
        return ResponseEntity.ok(parkingService.findAll());
    }
    @GetMapping("/{idPark}")
    public  ResponseEntity findParkingById(@PathVariable Long idPark){
        if (idPark==null){
            return ResponseEntity.badRequest().body("Empty request parameter");
        }
        Optional<ParkingDTO> parking= parkingService.findOne(idPark);

        if (!parking.isPresent()){
            return ResponseEntity.notFound().build();

        }
        return ResponseEntity.ok(parking);
    }


    @GetMapping("/{longitude}/{latitude}")
    public  ResponseEntity findParkingByLongLatt(@PathVariable double longitude, @PathVariable double latitude){
        if ((longitude == 0) || (latitude == 0)){
            return ResponseEntity.badRequest().body("Empty request parameter (longitude="+longitude+" latitude="+latitude+")");
        }
        Optional<ParkingDTO> parking= parkingService.findByLongitudeAndLatitude(longitude,latitude);
        if (!parking.isPresent()){
            return ResponseEntity.notFound().build();

        }

        return ResponseEntity.ok(parking);
    }


    @PostMapping("/")
    public ResponseEntity createParking(@RequestBody ParkingDTO parking){
        if (parking==null){
            return ResponseEntity.badRequest().body("Null Parking");
        }
        ParkingDTO createParking = parkingService.save(parking);

        return ResponseEntity.ok().body(createParking);
    }

    @DeleteMapping("/{idPark}")
    public  ResponseEntity DeleteParking (@PathVariable Long idPark){
        if (idPark==null){
            return ResponseEntity.badRequest().body("Null id parking");
        }
        Optional<ParkingDTO> parking = parkingService.findOne(idPark);

        parkingService.delete(idPark);
        return ResponseEntity.ok("The parking n°"+idPark+" was deleted.");

    }


}
