package com.gfi.parking.parking.DTO.mapper;

import com.gfi.parking.parking.DTO.PersonneDTO;
import com.gfi.parking.parking.entities.Adresse;
import com.gfi.parking.parking.entities.Personne;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;


@Mapper(componentModel = "spring", uses = {Adresse.class})
public interface PersonneMapper extends EntityMapper<PersonneDTO,Personne> {

    @Mappings({
            @Mapping(source =  "adresse.idAdr", target = "idAdr")

    })
    PersonneDTO toDto(Personne entity);


    @Mappings({
            @Mapping(source =  "idAdr", target = "adresse.idAdr"),
    })
    Personne toEntity(PersonneDTO dto);



   default Personne fromId(String id) {
        if (id == null) {
            return null;
        }
        Personne generated = new Personne();
        generated.setCin(id);
        return generated;
    }
}
