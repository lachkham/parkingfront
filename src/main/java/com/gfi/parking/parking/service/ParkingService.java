package com.gfi.parking.parking.service;

import com.gfi.parking.parking.DTO.ParkingDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ParkingService {
    ParkingDTO save(ParkingDTO parkingDTO);
    List<ParkingDTO> findAll();
    Optional<ParkingDTO> findOne(Long id);
    void delete(Long id);
    Optional<ParkingDTO> findByLongitudeAndLatitude(double longitude, double latitude);
}
