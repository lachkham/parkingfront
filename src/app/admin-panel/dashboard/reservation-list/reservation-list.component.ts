import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Parking } from 'src/app/models/parkingModel';
import { ParkingService } from 'src/app/service/parking/parking.service';
import { ReservationService } from 'src/app/service/parking/reservation/reservation.service';
import { AddReservationComponent } from 'src/app/add-reservation/add-reservation.component';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Reservation } from 'src/app/models/reservationModel';
import { DialogDeleteComponent } from '../parkings-list/dialog-delete/dialog-delete.component';
export interface DialogData {
  latitude;
   longitude;
   idParking;
   reservation:Reservation;
}
@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.css']
})
export class ReservationListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Parking>; 

  lat;
  long;
  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: Position) => {
        if (position) {
          console.log("Latitude: " + position.coords.latitude +
            "Longitude: " + position.coords.longitude);
          this.lat = position.coords.latitude;
          this.long = position.coords.longitude;
        }
      },
        (error: PositionError) => console.log(error));
    } else {
      alert("Geolocation is not supported by this browser.");
    }
    return true;
  }


  /////////////////////////////////////////////////
  openUpdateDialog(row:Reservation): void {
    const dialogRef = this.dialog.open(AddReservationComponent, {
      width: '100em',
      data:{latitude:this.lat,longitude:this.long,idParking:row.idParking,reservation:row}
    });

    dialogRef.afterClosed().subscribe(result => {
    if(result){
      this.refresh();
      }
    });
  }
  openDialog(lib,row:Reservation): void {
    const dialogRef = this.dialog.open(DialogDeleteComponent, {
      width: '350px',
      data: lib
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result){
        this.dataSource.data.splice(this.dataSource.data.indexOf(row),1);
        this.dataSource= new MatTableDataSource(this.dataSource.data);
        this.table.dataSource = this.dataSource;
        this.reservationService.DeleteOne(row.idReservation).subscribe();this.refresh()
        }
    });
  }

  //////////////////////////////////////////////////
  refresh(){
    if(this.router.url=="/admin/dashboard"){
      this.router.navigateByUrl('/admin', {skipLocationChange: true}).then(()=>
       this.router.navigate(["/admin/dashboard"])); }else{
         this.router.navigateByUrl('/admin', {skipLocationChange: true}).then(()=>
         this.router.navigate(["/admin/dashboard"]));
       }
  }
  

  dataSource = new MatTableDataSource([]);
  myDate = new Date();
  
  constructor(private parkingService : ParkingService,private reservationService:ReservationService,public dialog: MatDialog,private router: Router){
  }

  displayedColumns = ['Numero','libelle','dateEntre','dateSortie','Valider'];

  ngOnInit() {
    this.getLocation();
    
  }

  parkings;
  ngAfterViewInit() {
    this.reservationService.findAll().subscribe(allitem =>{
        allitem[0].idReservation;

      this.dataSource = new MatTableDataSource(allitem);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.table.dataSource = this.dataSource;
      
    },
    err=>{},
    ()=>{
      for (let i = 0;i < this.dataSource.filteredData.length;i++){
          this.parkingService.getOne(this.dataSource.filteredData[i].idParking).subscribe(park=>{
            this.dataSource.filteredData[i].parking=park;
          })
      }
    }
    )
  }

  getDate(datems){
    return new Date(datems);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}