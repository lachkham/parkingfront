import { Component, OnInit, ViewChild, ElementRef, Input, OnChanges, SimpleChanges, ɵConsole } from '@angular/core';
import Map from 'ol/Map';
import Tile from 'ol/layer/Tile';
import OSM from 'ol/source/OSM'
import View from 'ol/View';
import { fromLonLat, transform, toLonLat } from 'ol/proj';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import {Icon, Style} from 'ol/style';
import VectorSource from 'ol/source/Vector';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';
import TileJSON from 'ol/source/TileJSON';
import { ThrowStmt } from '@angular/compiler';
import { ParkingService } from '../service/parking/parking.service';
import { all } from 'q';
import WKT from 'ol/format/WKT'
import { Overlay } from 'ol';
import { Parking } from '../models/parkingModel';
import { toStringHDMS } from 'ol/coordinate';
import { tryParse } from 'selenium-webdriver/http';
import { AdresseService } from '../service/adresse/adresse.service';
import { Adresse } from '../models/adresseModel';
import { TypeParking } from '../models/typeParkingModel';
import { TypeParkingService } from '../service/parking/typeParking/type-parking.service';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { AddReservationComponent } from '../add-reservation/add-reservation.component';

@Component({
  selector: 'app-maps-component',
  templateUrl: './maps-component.component.html',
  styleUrls: ['./maps-component.component.css']
})

export class MapsComponentComponent implements OnInit ,OnChanges{
        map;
        parkingInfo:Parking;
        adressInfo:Adresse;
        typeInfo:TypeParking;
        parkingAvailable=false;
@Input() latitude: number;
@Input() longitude: number;

ngOnChanges(changes: SimpleChanges): void {
  if (this.longitude!=null && this.latitude!=null){
    this.parkingAvailable=false;
    this.isCurrentPosition=false;
    this.getParkingFromLongLat(this.longitude,this.latitude);
    this.overlayGlobal.setPosition(fromLonLat([this.longitude,this.latitude]));
  }
}


 lat=0;
 long=0;
  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: Position) => {
        if (position) {
          console.log("Latitude: " + position.coords.latitude +
            "Longitude: " + position.coords.longitude);
          this.lat = position.coords.latitude;
          this.long = position.coords.longitude;
          this.trackMe();

        }
      },
        (error: PositionError) => console.log(error));
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }




  constructor(private http : HttpClient,
    private parkingService : ParkingService,
    private adresseService :AdresseService,
    private typeParkingService:TypeParkingService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.getLocation();

  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.initializeMap();
}

  myPosition;
  vectorSource;
  vectorLayer;
  rasterLayer;
  markers:Feature[] =[];
  overlayGlobal;
  isCurrentPosition=false;
  initializeMap(){
    const content = document.getElementById('popup-content');
    const closer = document.getElementById('popup-closer');
    const container = document.getElementById('popup');
    const overlay = new Overlay({
      element: container,
      autoPanMargin:100,
      autoPan: true,
      autoPanAnimation: {
        duration: 1000
        
      }
    });
   this.overlayGlobal=overlay;

  setTimeout(()=>{this.parkingService.findAll().subscribe(parkings =>{      

    var parkingPosition;
    for (let i=0;i<parkings.length; i++){
        parkingPosition=new Feature({
          geometry: new Point(fromLonLat([parkings[i].longitude,parkings[i].latitude]))
        })  
        parkingPosition.setStyle(new Style({
          image: new Icon(({
            color: [30, 136, 229],
            crossOrigin: 'anonymous',
            src: 'assets/baseline-local_parking-24px.svg',
            imgSize: [32, 32]
          }
          
          ))
    
        }));
      
        

        this.markers.push(parkingPosition);
        console.log([parkings[i].longitude,parkings[i].latitude])
       
      }

      console.log("done!");
      this.vectorSource= new VectorSource({
          features : this.markers
        })
    
        
        this.vectorLayer = new VectorLayer({
          source: this.vectorSource
        });

        this.rasterLayer = new Tile({
          source: new OSM()
         });

    this.map = new Map({
      overlays:[overlay],
      target:'map',
      layers: [this.rasterLayer,this.vectorLayer],
      
      view  : new View({
        center:fromLonLat([this.long,this.lat]),
        zoom:10,
      })
    });
    this.map.on('singleclick',  (event) =>{
        this.parkingAvailable=false;
        this.isCurrentPosition=false;

      if (this.map.hasFeatureAtPixel(event.pixel) === true) {
          var coordinate = event.coordinate;
          this.map.forEachFeatureAtPixel(event.pixel,(feature)=>{
            var coord = feature.getGeometry().getCoordinates();
            coord = transform(coord, 'EPSG:3857', 'EPSG:4326');
            var lon = coord[0];
            var lat = coord[1];

            if(feature.values_.name=="My position")
                this.isCurrentPosition=true;
            else
            this.getParkingFromLongLat(lon,lat);
            

                console.log(coord);
          })
          overlay.setPosition(coordinate);
      } else {
          overlay.setPosition(undefined);
          closer.blur();
      }
  });
  this.map.on('pointermove', (event) =>{
    var pixel = this.map.getEventPixel(event.originalEvent);
    var hit = this.map.hasFeatureAtPixel(pixel);
    this.map.getViewport().style.cursor = hit ? 'pointer' : 'grab';
  });
  this.map.on('pointerdrag', (event) =>{
    this.map.getViewport().style.cursor = 'grabbing';
  });


  
    closer.onclick =  () =>{
      overlay.setPosition(undefined);
      closer.blur();
      this.parkingAvailable=false;
      this.isCurrentPosition=false;
      return false;
    };
    
    }
    );
  },500);

  }






  isTracking=true;
  trackMe() {
    if (navigator.geolocation) {
      navigator.geolocation.watchPosition((position) => {
        this.long=position.coords.longitude;
        this.lat=position.coords.latitude;
        this.trackmyposition();

        if(this.isTracking){
          this.map.setView(new View({
            center: fromLonLat([this.long,this.lat]),
            zoom: 15,
            }));
            this.isTracking = false;

        }
       
        
         
        
        
        });
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }
   trackmyposition() {
        if (this.myPosition!=null)
        this.vectorSource.removeFeature(this.myPosition);
       

     this.myPosition = new Feature({
      geometry: new Point(fromLonLat([this.long,this.lat])),
      name: 'My position',
      
    });
    this.vectorSource.addFeature(this.myPosition);
  }

    
    getParkingFromLongLat(Long,Lat){
       this.parkingService.getOneFromLongLat(Long,Lat).subscribe( (parking) => {
             this.parkingInfo = parking;
      },
        err =>{},
        ()=>{
          this.parkingAvailable = true;
          //adresse
          this.adresseService.getOne(this.parkingInfo.idAdr).subscribe(adr=>{
                      this.adressInfo=adr;
          });

          //type parking
          this.typeParkingService.getOne(this.parkingInfo.typeId).subscribe(type =>{
                      this.typeInfo=type;
          })
          
        } 
      )
    }

   
    openAddReservation(): void {
      const dialogRef = this.dialog.open(AddReservationComponent, {
        width: '100em',
        data: {latitude: this.lat, longitude: this.long,idParking: this.parkingInfo.idParking},
        
      });
  
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
       
      });
    } 


  }



