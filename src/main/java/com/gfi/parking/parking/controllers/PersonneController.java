package com.gfi.parking.parking.controllers;


import com.gfi.parking.parking.DTO.PersonneDTO;
import com.gfi.parking.parking.service.PersonneService;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/v1/personnes")
@CrossOrigin(origins = "*")

public class PersonneController {


    private final PersonneService personneService;

    public PersonneController(PersonneService personneService) {
        this.personneService = personneService;
    }

    //Get All Personne
    @GetMapping("/")
    public ResponseEntity findAll(){
        return  ResponseEntity.ok(personneService.findAll());
    }

    //Get personne By CIN
    @GetMapping("/{cin}")
    public ResponseEntity   findPersonneById(@PathVariable( name= "cin" ) String cin){

        if (cin==null)
            return ResponseEntity.badRequest().body("Error CIN is null");

        Optional<PersonneDTO> personne =personneService.findOne(cin);

        if (!personne.isPresent())
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok().body(personne);
    }

    //Add personne
    @PostMapping("/")
    public ResponseEntity createPersonne(@RequestBody PersonneDTO personne){
        if (personne==null)
            return ResponseEntity.badRequest().body("Cannot create NULL personne");
        PersonneDTO createPersonne= personneService.save(personne);

        return ResponseEntity.ok(createPersonne);
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestParam(name= "username") String username,@RequestParam(name = "pwd") String pwd){
        if(StringUtils.isEmpty(username))
            return ResponseEntity.badRequest().body("Empty username/email field");

        if(StringUtils.isEmpty(pwd))
            return ResponseEntity.badRequest().body("Empty password field");

        Optional<PersonneDTO> authentificatePersonne = personneService.findByUsernameAndPassword(username,pwd);

        if (!authentificatePersonne.isPresent())
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok(authentificatePersonne);


    }


}
