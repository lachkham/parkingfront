package com.gfi.parking.parking.repository;

import com.gfi.parking.parking.entities.Parking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface ParkingRepository extends JpaRepository<Parking,Long> {
     Parking findByLongitudeAndLatitude(double longitude, double latitude);
     Parking findByLongitudeBetweenAndLatitudeBetween(double LongInf, double LongSup,double LattInf, double LattSup);

}
