package com.gfi.parking.parking.entities;



import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
public class Personne implements Serializable {

    @Id
    private String cin ;
    /*@Lob
    @Nullable
    private byte[] photo;*/
    private String prenom;
    private String nom;
    private Date DateNaiss;

    @ManyToOne
    private Adresse adresse;

    private String username;
    private String password;
    private String Email;


}
