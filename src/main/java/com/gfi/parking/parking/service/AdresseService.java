package com.gfi.parking.parking.service;

import com.gfi.parking.parking.DTO.AdresseDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface AdresseService {
    /**
     * Save a Address.
     *
     * @param adresseDTO the entity to save
     * @return the persisted entity
     */
    AdresseDTO save(AdresseDTO adresseDTO);
    /**
     * Get all the Addresses.
     *
     * @return the list of entities
     */
    List<AdresseDTO> findAll();
    /**
     * Get the "id" Address.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<AdresseDTO> findOne(Long id);

    /**
     * Delete the "id" Address.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
