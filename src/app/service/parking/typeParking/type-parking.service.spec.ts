import { TestBed } from '@angular/core/testing';

import { TypeParkingService } from './type-parking.service';

describe('TypeParkingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypeParkingService = TestBed.get(TypeParkingService);
    expect(service).toBeTruthy();
  });
});
