package com.gfi.parking.parking.service.impl;

import com.gfi.parking.parking.DTO.TypeParkingDTO;
import com.gfi.parking.parking.DTO.mapper.TypeParkingMapper;
import com.gfi.parking.parking.repository.TypeParkingRepository;
import com.gfi.parking.parking.service.TypeParkingService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TypeParkingServiceImpl implements TypeParkingService {

    private  final TypeParkingMapper typeParkingMapper;
    private final TypeParkingRepository typeParkingRepository;


    public TypeParkingServiceImpl(TypeParkingMapper typeParkingMapper, TypeParkingRepository typeParkingRepository) {
        this.typeParkingMapper = typeParkingMapper;
        this.typeParkingRepository = typeParkingRepository;
    }


    @Override
    public TypeParkingDTO save(TypeParkingDTO typeParkingDTO) {
        return typeParkingMapper.toDto(typeParkingRepository.save(typeParkingMapper.toEntity(typeParkingDTO)));
    }

    @Override
    @Transactional(readOnly = true)
    public List<TypeParkingDTO> findAll() {
        return typeParkingMapper.toDto(typeParkingRepository.findAll());
    }

    @Override
    public Optional<TypeParkingDTO> findOne(Long id) {
        return Optional.of(typeParkingMapper.toDto(typeParkingRepository.findById(id).get()));
    }

    @Override
    public void delete(Long id) {
        typeParkingRepository.deleteById(id);

    }
}
