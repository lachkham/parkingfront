import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTypeParkingComponent } from './add-type-parking.component';

describe('AddTypeParkingComponent', () => {
  let component: AddTypeParkingComponent;
  let fixture: ComponentFixture<AddTypeParkingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTypeParkingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTypeParkingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
