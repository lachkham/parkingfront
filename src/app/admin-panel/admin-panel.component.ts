import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';
import { ParkingService } from '../service/parking/parking.service';
import { LoginService } from '../service/personne/login.service';
import { longStackSupport } from 'q';
import { Router } from '@angular/router';
import { PersonneService } from '../service/personne/personne.service';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent {
  opned=true;
  validation;
  Admin=false;
  ngOnInit(): void {
    
    this.userService.getCurrentUser().subscribe(res=>{
      localStorage.setItem("user", res.username);
      if(res.username==res.superior){
        localStorage.setItem("Role", "Admin");
        this.Admin=true;
      }
      else{
        localStorage.setItem("Role", "Employee");
        this.Admin=false;
      }

      console.log(res);
      console.log(localStorage);
    });

    this.loginService.checkSession().subscribe(
      res =>   {
        console.log("===================================== true:"+res);
      },
      error => {
        console.log("===================================== false:"+error);

        
      }
    )


///////////////////////////////////////    
    window.innerWidth;
    if (window.innerWidth<=720){
        this.opned=false;
    }else{
        this.opned=true;
    }
    this.countValidation();
  }
  onResize(event?) {
    window.innerWidth;
    if (window.innerWidth<=720){
        this.opned=false;
    }else{
        this.opned=true;
    }
 
 }

    constructor(private parkinService: ParkingService,private loginService: LoginService, private router : Router, private userService:PersonneService) {}

    countValidation(){
        this.parkinService.countInvalid().subscribe(res =>{
            if (res>0){
              this.validation=res;
            }
        });
    }

    logout(){
      console.log(localStorage.getItem('xAuthToken'));
      this.loginService.logout().subscribe(res=>{
        console.log("logged out: ",res);
        this.router.navigate(['/']);

      },
      (error)=>{
        console.error(error);
      });

    }

}
