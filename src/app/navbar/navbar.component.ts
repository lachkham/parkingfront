import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddParkingComponent } from '../add-parking/add-parking.component';
import { AddReservationComponent } from '../add-reservation/add-reservation.component';
import { LoginService } from '../service/personne/login.service';
import { AddUserComponent } from '../admin-panel/gestion-admin/add-user/add-user.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  lat;
  long;
  constructor(public dialog: MatDialog,private loginService: LoginService,private router: Router) { }
  openAddReservation(): void {
    const dialogRef = this.dialog.open(AddReservationComponent, {
      width: '100em',
      data: {latitude: this.lat, longitude: this.long,idParking: null},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
     
    });
  }
  openAddParking(): void {
    const dialogRef = this.dialog.open(AddParkingComponent, {
      width: '100em'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
     
    });
    
  }
  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: Position) => {
        if (position) {
          console.log("Latitude: " + position.coords.latitude +
            "Longitude: " + position.coords.longitude);
          this.lat = position.coords.latitude;
          this.long = position.coords.longitude;
        }
      },
        (error: PositionError) => console.log(error));
    } else {
      alert("Geolocation is not supported by this browser.");
    }
    return true;
  }


  LoggedIn=false;

  logout(){
    console.log(localStorage.getItem('xAuthToken'));
    this.loginService.logout().subscribe(res=>{
      console.log("logged out: ",res);
      location.reload();
    },
    (error)=>{
      console.error(error);
    });

  }
  ngOnInit() {
    if(localStorage.getItem('xAuthToken')!=null){
      this.LoggedIn=true;
    }else{
      this.LoggedIn=false;
    }
    
    this.getLocation();
  }
  openAddDialog(): void {
    const dialogRef = this.dialog.open(AddUserComponent, {
      width: '100em',
      data:{admin:true,personne:null}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    if(result){
      this.router.navigate(["/login/"])      }
    });
  }
}
