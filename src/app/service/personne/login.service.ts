import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { AppSettings } from 'src/app/settings/app.settings';
import { Observable } from 'rxjs';
export interface Token {
	token: string;
  }

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  sendCredential(username : string, password : string) : Observable <Token>{
		let url = AppSettings.App_URL+"/token"; 
		let encodedCredentials = btoa(username+ ":" + password);
		let basicHeader = "Basic " + encodedCredentials; 
		let headers = new HttpHeaders({
			"Content-Type" : 'application/x-www-form-urlencoded',
			"Authorization" : basicHeader
		});
		console.log(headers);

  	return this.http.get<Token>(url, {headers: headers});
  }

  checkSession(){
	let url = AppSettings.App_URL+"/checkSession"; 
	let headers = new HttpHeaders({
		'x-auth-token' : localStorage.getItem('xAuthToken')
	});

	return this.http.get(url, {headers: headers});
}

logout(){
	let url = AppSettings.App_URL+"/user/logout"; 
	let headers = new HttpHeaders({
		'x-auth-token' : localStorage.getItem('xAuthToken')
	});
	localStorage.clear();
	return this.http.post(url,'', {headers: headers});
}




}
