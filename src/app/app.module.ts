import { BrowserModule } from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgModule, Component } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapsComponentComponent } from './maps-component/maps-component.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { AddTypeParkingComponent } from './add-type-parking/add-type-parking.component';
import { LoginComponent } from './login/login.component';
import { AdresseService } from './service/adresse/adresse.service';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { AddParkingComponent } from './add-parking/add-parking.component';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import { TypeParkingService } from './service/parking/typeParking/type-parking.service';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { LayoutModule } from '@angular/cdk/layout';
import { NavbarComponent } from './navbar/navbar.component';
import { ParkingTableauComponent } from './parking-tableau/parking-tableau.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { CarouselSlideComponent } from './carousel-slide/carousel-slide.component';
import { FooterComponent } from './footer/footer.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDialogModule} from '@angular/material/dialog';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { AddReservationComponent } from './add-reservation/add-reservation.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { RouterModule, Routes } from '@angular/router';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { DashboardComponent } from './admin-panel/dashboard/dashboard.component';
import {MatBadgeModule} from '@angular/material/badge';
import { ValidationComponent } from './admin-panel/dashboard/validation/validation.component';
import { ParkingsListComponent } from './admin-panel/dashboard/parkings-list/parkings-list.component';
import { ReservationListComponent } from './admin-panel/dashboard/reservation-list/reservation-list.component';
import { DialogDeleteComponent } from './admin-panel/dashboard/parkings-list/dialog-delete/dialog-delete.component';
import {RoundProgressModule} from 'angular-svg-round-progressbar';
import { AuthGuard } from './guards/auth-guard.service';
import { LoginGuard } from './guards/login-guard.service';
import { GestionAdminComponent } from './admin-panel/gestion-admin/gestion-admin.component';
import { UserListComponent } from './admin-panel/gestion-admin/user-list/user-list.component';
import { AddUserComponent } from './admin-panel/gestion-admin/add-user/add-user.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MyAccountComponent } from './admin-panel/my-account/my-account.component';
import { CardFormComponent } from './card-form/card-form.component';


@NgModule({
  declarations: [
    AppComponent,
    MapsComponentComponent,
    InscriptionComponent,
    AddParkingComponent,
    AddTypeParkingComponent,
    LoginComponent,
    HomeComponent,
    NavbarComponent,
    ParkingTableauComponent,
    CarouselSlideComponent,
    FooterComponent,
    AddReservationComponent,
    AdminPanelComponent,
    DashboardComponent,
    ValidationComponent,
    ParkingsListComponent,
    ReservationListComponent,
    DialogDeleteComponent,
    GestionAdminComponent,
    UserListComponent,
    AddUserComponent,
    MyAccountComponent,
    CardFormComponent        
  ],
  entryComponents: [
    NavbarComponent,
    AddParkingComponent,
    AddReservationComponent,
    DialogDeleteComponent,
    AddUserComponent  ],
  imports: [
    RouterModule.forRoot(
      [
        { path: 'home', component: HomeComponent },
        {path: 'login', component:LoginComponent,canActivate:[LoginGuard]},
        { path: 'admin', 
          component:   AdminPanelComponent,
          canActivate:[AuthGuard],
          children:[
            {
              path:'dashboard',
              component:DashboardComponent,
            },
            {
              path:'users',
              component:GestionAdminComponent,
            },
            {
              path:'account',
              component:MyAccountComponent
            },
            {
              path:'',
              component:DashboardComponent,
            },

        { path: '', component: HomeComponent },
        

      ],
        },
        { path: '', component: HomeComponent },
        

      ],
      
      { 
        useHash: true,
        onSameUrlNavigation: 'reload'
      } 
    ),
    BrowserModule,
    NgbModule, 
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    ReactiveFormsModule,
    MatGridListModule,
    MatMenuModule,
    MatIconModule,
    LayoutModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatBadgeModule,
    RoundProgressModule,
    MatDatepickerModule,
    MatNativeDateModule
    
  ],
  providers: [AdresseService,TypeParkingService,AuthGuard,LoginGuard,AddUserComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
 