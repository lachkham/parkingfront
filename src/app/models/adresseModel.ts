export class Adresse {
    idAdr       : number;
    adresse     : string
    codePostal  : string;
    ville       : string;
    gouvernorat : string;
    pays        : string;
}