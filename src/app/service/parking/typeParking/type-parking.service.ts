import { Injectable } from '@angular/core';
import { AppSettings } from 'src/app/settings/app.settings';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TypeParking } from 'src/app/models/typeParkingModel';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class TypeParkingService {
  headers;

  constructor(private http : HttpClient) { 
    if(localStorage.getItem('xAuthToken')!=null){
      this.headers = new HttpHeaders({'x-auth-token' : localStorage.getItem('xAuthToken')});
    }else{
      this.headers=null;
    }
  }


  findAll(): Observable <TypeParking[]>{
    return this.http.get<TypeParking[]>(AppSettings.App_URL+"/Type/", {headers: this.headers});
  }
  getOne(id): Observable <TypeParking>{
    return this.http.get<TypeParking>(AppSettings.App_URL+"/Type/"+id, {headers: this.headers});
  }
  AddOne(adresse : TypeParking): Observable <TypeParking>{
    return this.http.post <TypeParking>(AppSettings.App_URL+"/Type/",adresse, {headers: this.headers});
  }
  DeleteOne(id): Observable <TypeParking>{
    return this.http.delete <TypeParking>(AppSettings.App_URL+"/Type/"+id, {headers: this.headers});
  }
}
