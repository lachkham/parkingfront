import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Reservation } from 'src/app/models/reservationModel';
import { AppSettings } from 'src/app/settings/app.settings';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor(private http : HttpClient) { if(localStorage.getItem('xAuthToken')!=null){
    this.headers = new HttpHeaders({'x-auth-token' : localStorage.getItem('xAuthToken')});
  }else{
    this.headers=null;
  }}
  
  headers ;

  findAll(): Observable <Reservation[]>{
    return this.http.get<Reservation[]>(AppSettings.App_URL+"/reservation/", {headers: this.headers});
  }
  getOne(id): Observable <Reservation>{
    return this.http.get<Reservation>(AppSettings.App_URL+"/reservation/"+id, {headers: this.headers});
  }
  AddOne(reservation : Reservation): Observable <Reservation>{
    return this.http.post <Reservation>(AppSettings.App_URL+"/reservation/",reservation, {headers: this.headers});
  }
  DeleteOne(id): Observable <Reservation>{
    return this.http.delete <Reservation>(AppSettings.App_URL+"/reservation/"+id, {headers: this.headers});
  }

  nbReservation(id,entre,sortie): Observable <number>{
    return this.http.get<number>(AppSettings.App_URL+"/reservation/Get/"+id+"/"+entre+"/"+sortie, {headers: this.headers});
  }
}
