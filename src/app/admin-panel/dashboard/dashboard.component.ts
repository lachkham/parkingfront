import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { ParkingService } from 'src/app/service/parking/parking.service';
import { ReservationService } from 'src/app/service/parking/reservation/reservation.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit{
  myDate = new Date();


  constructor(private breakpointObserver: BreakpointObserver,private parkingService: ParkingService,private reservationSrvice:ReservationService) {}
  getCircleStyle() {
    let isSemi = false;
    let transform = (isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

    return {
      'top': isSemi ? 'auto' : '50%',
      'bottom': isSemi ? '5%' : 'auto',
      'left': '50%',
      'transform': transform,
      '-moz-transform': transform,
      '-webkit-transform': transform,
      'font-size': 60 / 3.5 + 'px'
    };
  }

  getSemiCircleStyle() {
    let isSemi = true;
    let transform = (isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';
    return {
      'top': isSemi ? 'auto' : '50%',
      'bottom': isSemi ? '30%' : 'auto',
      'left': '50%',
      'transform': transform,
      '-moz-transform': transform,
      '-webkit-transform': transform,
      'font-size': 120 / 3.5 + 'px'
    };
  }
  nbPlacetotal;
  nbParkingValid;
  parkActif;
  nbParkingActifs=0;
  nbReservationTotal;
  nbReservationInst;
  ngOnInit(): void {
    console.log(localStorage);

          this.reservationSrvice.nbReservation(-1,-1,-1).subscribe(res=>{
            this.nbReservationTotal=res;
          });

          this.reservationSrvice.nbReservation(-1,this.myDate.getTime(),0).subscribe(res=>{
            this.nbReservationInst=res;
          });

          this.parkingService.nbPlace().subscribe(res=>{
            this.nbPlacetotal=res;
          });
          this.parkingService.nbParkingActifs(this.myDate.getTime(),0).subscribe(res=>{
            this.nbParkingActifs=res;
            console.log("Parking Actifs:"+res);
          });

          this.parkingService.countValid().subscribe(res=>{
            this.nbParkingValid=res;
          });


          
  }
}
