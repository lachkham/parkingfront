import { Component, Inject, SimpleChanges, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { AdresseService } from 'src/app/service/adresse/adresse.service';
import { Adresse } from 'src/app/models/adresseModel';
import { Personne } from 'src/app/models/personneModel';
import { PersonneService } from 'src/app/service/personne/personne.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {
  states = [
    {name: 'Ariana', abbreviation: 'Ariana'},
    {name: 'Béja', abbreviation: 'Béja'},
    {name: 'Ben Arous', abbreviation: 'Ben Arous'},
    {name: 'Bizerte', abbreviation: 'Bizerte'},
    {name: 'Gabès', abbreviation: 'Gabès'},
    {name: 'Gafsa', abbreviation: 'Gafsa'},
    {name: 'Jendouba', abbreviation: 'Jendouba'},
    {name: 'Kairouan', abbreviation: 'Kairouan'},
    {name: 'Kasserine', abbreviation: 'Kasserine'},
    {name: 'Kebili', abbreviation: 'Kebili'}, 
    {name: 'Kef', abbreviation: 'Kef'},
    {name: 'Mahdia', abbreviation: 'Mahdia'},
    {name: 'Manouba', abbreviation: 'Manouba'},
    {name: 'Medenine', abbreviation: 'Medenine'},
    {name: 'Monastir', abbreviation: 'Monastir'},
    {name: 'Nabeul', abbreviation: 'Nabeul'},
    {name: 'Sfax', abbreviation: 'Sfax'},
    {name: 'Sidi Bouzid', abbreviation: 'Sidi Bouzid'},
    {name: 'Siliana', abbreviation: 'Siliana'},
    {name: 'Sousse', abbreviation: 'Sousse'},
    {name: 'Tataouine', abbreviation: 'Tataouine'},
    {name: 'Tozeur', abbreviation: 'Tozeur'},
    {name: 'Tunis', abbreviation: 'Tunis'},
    {name: 'Zaghouan', abbreviation: 'Zaghouan'}
  ];
    date:String;
ngOnChanges(changes: SimpleChanges): void {
 
  this.date = new Date().toISOString().slice(0, 16);

}
  addressForm = this.fb.group({
    adresse: [null, Validators.required],
    Ville: [null, Validators.required],
    Gouvernorat: [null, Validators.required],
    Pays: [null, Validators.required],
    postalCode: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(5)])],
   
    cin:new FormControl({ value: '', disabled: true }),
    prenom:[null, Validators.required],
    nom:[null, Validators.required],
    DateNaiss:new FormControl((new Date()).toISOString()),
    Emailuser:new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
  
    username:[null, Validators.required],
    password:new FormControl()

  });




  hasUnitNumber = false;

 


  constructor(private fb: FormBuilder,
    private adresseService : AdresseService,
    private personneService: PersonneService,
    private router: Router,
    private _snackBar: MatSnackBar){}
data:Personne;
  ngOnInit(): void {
   this.personneService.getCurrentUser().subscribe(res=>{
     this.data=res;
   },err=>{},()=>{

    if (this.data!=null){
      console.log(this.data.username);
    this.addressForm.get('cin').setValue(this.data.cin);  
    this.addressForm.get('prenom').setValue(this.data.prenom);  
    this.addressForm.get('nom').setValue(this.data.nom);  
    this.addressForm.get('Emailuser').setValue(this.data.email);  
    this.addressForm.get('DateNaiss').setValue(new Date(this.data.dateNaiss)); 
    this.addressForm.get('username').setValue(this.data.username);

      
    this.adresseService.getOne(this.data.idAdr).subscribe(res =>{
      
      this.addressForm.get('adresse').setValue(res.adresse); 
      this.addressForm.get('Ville').setValue(res.ville); 
      this.addressForm.get('Gouvernorat').setValue(res.gouvernorat); 
      this.addressForm.get('Pays').setValue(res.pays); 
      this.addressForm.get('postalCode').setValue(res.codePostal); 
    })  
  } 

   });
    
  }
  onSubmit() {

    
    let personne = new Personne();
    let adresse = new Adresse();
    

    adresse.idAdr         =this.data.idAdr;
    personne.cin          =this.data.cin;
    personne.prenom       =this.addressForm.get('prenom').value;
    personne.nom          =this.addressForm.get('nom').value
    personne.dateNaiss    =this.addressForm.get('DateNaiss').value;
    personne.email        =this.addressForm.get('Emailuser').value;
    personne.username     =this.addressForm.get('username').value;
    personne.status       =true;
     
    if(this.addressForm.get('password').value==null || this.addressForm.get('password').value ==''){
        personne.password = this.data.password;
      }else{
        personne.password = this.addressForm.get('password').value
      }


    adresse.adresse =this.addressForm.get("adresse").value;
    adresse.ville =this.addressForm.get("Ville").value;
    adresse.gouvernorat =this.addressForm.get("Gouvernorat").value;
    adresse.pays =this.addressForm.get("Pays").value;
    adresse.codePostal =this.addressForm.get("postalCode").value;

if(personne.prenom!=null&&personne.nom!=null&&personne.dateNaiss!=null&&personne.email!=null&&personne.username!=null&&personne.cin!=null
   && adresse.adresse!=null&&adresse.ville!=null&&adresse.gouvernorat!=null&&adresse.pays!=null&&adresse.codePostal!=null){
    
    
    
    this.adresseService.AddOne(adresse).subscribe(
      data => {
        personne.idAdr     = data.idAdr;
      },
      err =>{ this._snackBar.open("Erreur !","Fermer", {
        duration: 2000,
      });
    },
      ()=>{

          this.personneService.AddOne(personne).subscribe(data=>{
            this._snackBar.open("Modifier avec succés","Fermer", {
              duration: 2000,
            });
            console.log(data);
          },
          err=>{
            this._snackBar.open("Erreur !","Fermer", {
              duration: 2000,
            });
          },
          ()=>{
            this.refresh();
          });


      }
    );


   } 
  }

  refresh(){
    if(this.router.url=="/admin/account"){
      this.router.navigateByUrl('/admin', {skipLocationChange: true}).then(()=>
       this.router.navigate(["/admin/account"])); 
      }
       }
  

}
