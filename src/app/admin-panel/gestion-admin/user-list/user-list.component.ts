import { AfterViewInit, Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Parking } from 'src/app/models/parkingModel';
import { ParkingService } from 'src/app/service/parking/parking.service';
import { ReservationService } from 'src/app/service/parking/reservation/reservation.service';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DialogDeleteComponent } from '../../dashboard/parkings-list/dialog-delete/dialog-delete.component';
import { PersonneService } from 'src/app/service/personne/personne.service';
import { AddUserComponent } from '../add-user/add-user.component';
import { LoginService } from 'src/app/service/personne/login.service';
import { Personne } from 'src/app/models/personneModel';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Parking>; 

    


  

  dataSource = new MatTableDataSource([]);
  myDate = new Date();
  
  constructor(private loginService: LoginService,private userService : PersonneService,private reservationService:ReservationService, public dialog: MatDialog,private router: Router){
  }

  openDialog(lib,row:Personne): void {
    console.log(row.cin);
    let thisUser:Personne;
    
    this.userService.getCurrentUser().subscribe(res=>{
      thisUser=res;
    },err=>{},
    ()=>{
      this.userService.EnableDisable(row).subscribe(()=>{},()=>{},()=>{
        if(row.cin==thisUser.cin)
        this.logout();
        else
        this.refresh()});
    })
  }


refresh(){
  
  if(this.router.url=="/admin/users"){
    this.router.navigateByUrl('/admin', {skipLocationChange: true}).then(()=>
     this.router.navigate(["/admin/users"])); }else{
       this.router.navigateByUrl('/admin', {skipLocationChange: true}).then(()=>
       this.router.navigate(["/admin/users"]));
     }
}


  openAddDialog(): void {
    const dialogRef = this.dialog.open(AddUserComponent, {
      width: '100em',
      data:{admin:false,personne:null}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    if(result){
      this.refresh();
      }
    });
  }


  openUpdateDialog(row): void {
    const dialogRef = this.dialog.open(AddUserComponent, {
      width: '100em',
      data:{admin:false,personne:row}
    });
    
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    if(result){
      this.refresh();
      }
    });
  }




  displayedColumns = ['nom','prenom','username','dateNaiss','status','Supprimer'];

  ngOnInit() {
  }
  parkings;
  ngAfterViewInit() {
      this.userService.findAll().subscribe(users=>{
      this.dataSource = new MatTableDataSource(users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.table.dataSource = this.dataSource;
      })

 
  }

  logout(){
    console.log(localStorage.getItem('xAuthToken'));
    this.loginService.logout().subscribe(res=>{
      console.log("logged out: ",res);
      this.router.navigate(['/']);

    },
    (error)=>{
      console.error(error);
    });

  }
  


  

  
}
 

