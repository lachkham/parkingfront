import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import {map} from 'rxjs/operators'
import { Observable } from 'rxjs';
import { LoginService } from '../service/personne/login.service';
@Injectable()
export class AuthGuard implements CanActivate {
  constructor (private auth: LoginService , 
                private router : Router){}
  loggedIn

  canActivate(next: ActivatedRouteSnapshot,state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    
    
    if(localStorage.getItem('xAuthToken')!=null){
      return true
    }
    this.router.navigate(['/login']);
    return false;

  }
}

