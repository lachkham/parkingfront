import { Component, HostListener } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  screenWidth: number;
  col1 =1;
  col3=3;
  @HostListener('window:resize', ['$event'])
onResize(event?) {
   window.innerWidth;
   if (window.innerWidth<=720){
     this.col1=4;
     this.col3=4;
   }else{
    this.col1=1;
    this.col3=3;
   }

}

  constructor() {
    this.onResize();

  }
  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class
  }

   lat;
   long;

  receiveCoord($event){
        this.lat=$event[0];
        this.long=$event[1];
  }




  
  }
  
  
