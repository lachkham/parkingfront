package com.gfi.parking.parking.service.impl;

import com.gfi.parking.parking.DTO.PersonneDTO;
import com.gfi.parking.parking.DTO.mapper.PersonneMapper;
import com.gfi.parking.parking.repository.PersonneRepository;
import com.gfi.parking.parking.service.PersonneService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class PersonneServiceImpl implements PersonneService {

    private final PersonneMapper personneMapper;
    private final PersonneRepository personneRepository;

    public PersonneServiceImpl(PersonneMapper personneMapper, PersonneRepository personneRepository) {
        this.personneMapper = personneMapper;
        this.personneRepository = personneRepository;
    }


    @Override
    public PersonneDTO save(PersonneDTO personneDTO) {
        return personneMapper.toDto(personneRepository.save(personneMapper.toEntity(personneDTO)));
    }

    @Override

    public List<PersonneDTO> findAll() {
        return personneMapper.toDto(personneRepository.findAll());

    }

    @Override
    public Optional<PersonneDTO> findOne(String id) {
        return Optional.of(personneMapper.toDto(personneRepository.findById(id).get()));
    }

    @Override
    public void delete(String id) {
        personneRepository.deleteById(id);

    }

    @Override
    public Optional<PersonneDTO> findByUsernameAndPassword(String username, String password) {

        return Optional.of(personneMapper.toDto(personneRepository.findByUsernameAndPassword(username,password)));
    }

}