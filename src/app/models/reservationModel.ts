import { Parking } from './parkingModel';

export class Reservation{
      idReservation :number;
      entre         :number;
      sortie        :number;
      idParking     :number;
      parking       :Parking
      email         :String;
}