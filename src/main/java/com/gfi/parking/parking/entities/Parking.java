package com.gfi.parking.parking.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;


@Entity
@Data
public class Parking implements Serializable {
    @Id
    @GeneratedValue
    private Long idParking ;
    private String libelle;
    private  double longitude;
    private  double latitude;
    private int nbPlace;
    @ManyToOne
    private Adresse adresse;
    @ManyToOne
    private TypeParking type;



}
