import { Injectable } from '@angular/core';
import { AppSettings } from 'src/app/settings/app.settings';
import { Parking } from 'src/app/models/parkingModel';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ReturnStatement } from '@angular/compiler';
@Injectable({
  providedIn: 'root'
})
export class ParkingService {
  headers;
  constructor(private http : HttpClient) {
    if(localStorage.getItem('xAuthToken')!=null){
    this.headers = new HttpHeaders({'x-auth-token' : localStorage.getItem('xAuthToken')});
  }else{
    this.headers=null;
  }

   }

/*   http://localhost:8080/v1/reservation/ParkingActifs/0/0

;
 */

  findAll(): Observable<Parking[]>{
   
    return this.http.get<Parking[]>(AppSettings.App_URL+"/parking/Get/", {headers: this.headers});
  }

  findAllByOwner(): Observable<Parking[]>{
   
    return this.http.get<Parking[]>(AppSettings.App_URL+"/parking/All/ByOwner/", {headers: this.headers});
  }
  findAllinvalid(): Observable<Parking[]>{
    return this.http.get<Parking[]>(AppSettings.App_URL+"/parking/invalid",{headers: this.headers});
  }
  getOne(id): Observable <Parking>{
    return this.http.get<Parking>(AppSettings.App_URL+"/parking/Get/"+id,{headers: this.headers});
  }
  AddOne(adresse : Parking): Observable <Parking>{
    return this.http.post<Parking>(AppSettings.App_URL+"/parking/",adresse,{headers: this.headers});
  }
  DeleteOne(id): Observable <Parking>{
    return this.http.delete<Parking>(AppSettings.App_URL+"/parking/"+id,{headers: this.headers});
  }
  getOneFromLongLat(Long,Lat):Observable<Parking>{
    return this.http.get<Parking>(AppSettings.App_URL+"/parking/Get/"+Long+"/"+Lat,{headers: this.headers});
  }
  countInvalid():Observable<number>{
    return this.http.get<number>(AppSettings.App_URL+"/parking/count/invalid",{headers: this.headers});
  }
  countValid():Observable<number>{
    return this.http.get<number>(AppSettings.App_URL+"/parking/count/valid",{headers: this.headers});
  }
  

  nbPlace():Observable<number>{
    return this.http.get<number>(AppSettings.App_URL+"/parking/nbPlace",{headers: this.headers});
  }

  nbParkingActifs(entre,sortie):Observable<number>{
    return this.http.get<number>(AppSettings.App_URL+"/reservation/ParkingActifs/"+entre+"/"+sortie,{headers: this.headers});
  }

  Valider(row):Observable<Parking>{

if(this.headers!=null)

    return this.http.get<Parking>(AppSettings.App_URL+"/parking/validation/"+row,{headers: this.headers});
  } 
}

