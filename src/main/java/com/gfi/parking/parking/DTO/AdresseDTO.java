package com.gfi.parking.parking.DTO;

import lombok.Data;

@Data
public class AdresseDTO {

    private Long   idAdr;
    private String Adresse;
    private String codePostal;
    private String Ville;
    private String Gouvernorat;
    private String pays;

}
