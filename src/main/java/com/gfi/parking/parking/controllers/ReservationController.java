package com.gfi.parking.parking.controllers;


import com.gfi.parking.parking.DTO.ReservationDTO;
import com.gfi.parking.parking.service.ReservationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/v1/reservation")
@CrossOrigin(origins = "*")
public class ReservationController {
    private final ReservationService reservationService;

    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }


    @GetMapping("/")
    public ResponseEntity findAll(){
        return ResponseEntity.ok(reservationService.findAll());

    }
    @GetMapping("/{idType}")
    public ResponseEntity findByID(@PathVariable Long idType){
        if (idType== null){
            return ResponseEntity.badRequest().body("Null Identifier");
        }
        Optional<ReservationDTO> typeParking = reservationService.findOne(idType);
        if (!typeParking.isPresent())
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok(typeParking);
    }

    @PostMapping("/")
    public ResponseEntity createReservation(@RequestBody ReservationDTO type){
        if(type==null){
            ResponseEntity.badRequest().body("Empty Request Body");
        }

        ReservationDTO typeParking = reservationService.save(type);

        return ResponseEntity.ok(typeParking);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteReservation(@PathVariable Long id){
        if (id==null){
            return ResponseEntity.badRequest().body("Null id ");
        }
        Optional<ReservationDTO> Type = reservationService.findOne(id);

        reservationService.delete(id);
        return ResponseEntity.ok("");

    }
    @GetMapping("{idPark}/{entre}/{sortie}")
    public ResponseEntity countReservation(@PathVariable Long idPark,@PathVariable Long entre,@PathVariable Long sortie){
        if(entre==null||sortie==null){
            return ResponseEntity.badRequest().body("null Request");
        }
        Optional<Long> nbReservation = reservationService.nbReservation(idPark,entre,sortie);
        System.out.println(nbReservation);
        return ResponseEntity.ok(nbReservation);
    }
}
