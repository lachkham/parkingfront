import { AfterViewInit, Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Parking } from 'src/app/models/parkingModel';
import { ParkingService } from 'src/app/service/parking/parking.service';
import { ReservationService } from 'src/app/service/parking/reservation/reservation.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogDeleteComponent } from './dialog-delete/dialog-delete.component';
import { AddParkingComponent } from 'src/app/add-parking/add-parking.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-parkings-list',
  templateUrl: './parkings-list.component.html',
  styleUrls: ['./parkings-list.component.css']
})
export class ParkingsListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Parking>; 

    


  

  dataSource = new MatTableDataSource([]);
  myDate = new Date();
  
  constructor(private parkingService : ParkingService,private reservationService:ReservationService, public dialog: MatDialog,private router: Router){
  }

  openDialog(lib,row): void {
    const dialogRef = this.dialog.open(DialogDeleteComponent, {
      width: '350px',
      data: lib
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result){
        this.dataSource.data.splice(this.dataSource.data.indexOf(row),1);
        this.dataSource= new MatTableDataSource(this.dataSource.data);
        this.table.dataSource = this.dataSource;
        this.parkingService.DeleteOne(row.idParking).subscribe();this.refresh()
        }
    });
  }
refresh(){
  if(this.router.url=="/admin/dashboard"){
    this.router.navigateByUrl('/admin', {skipLocationChange: true}).then(()=>
     this.router.navigate(["/admin/dashboard"])); }else{
       this.router.navigateByUrl('/admin', {skipLocationChange: true}).then(()=>
       this.router.navigate(["/admin/dashboard"]));
     }
}
  openAddDialog(): void {
    const dialogRef = this.dialog.open(AddParkingComponent, {
      width: '100em',
      data:null
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    if(result){
      this.refresh();
      }
    });
  }
  openUpdateDialog(row): void {
    const dialogRef = this.dialog.open(AddParkingComponent, {
      width: '100em',
      data:row
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    if(result){
      this.refresh();
      }
    });
  }




  displayedColumns = ['libelle','distance','nbPlace','Supprimer'];

  ngOnInit() {
  }
  parkings;
  ngAfterViewInit() {
    this.parkingService.findAllByOwner().subscribe(allitem =>{
      this.dataSource = new MatTableDataSource(allitem);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.table.dataSource = this.dataSource;   
    },
    err=>{},
    ()=>{
      this.getLocation();
      for (let i = 0;i < this.dataSource.filteredData.length;i++){
          this.reservationService.nbReservation(this.dataSource.filteredData[i].idParking,this.myDate.getTime(),0).subscribe(nbplace=>{
            this.dataSource.filteredData[i].nbPlace=this.dataSource.filteredData[i].nbPlace-nbplace;
            console.log(this.dataSource.filteredData[i].idParking+" "+nbplace);
          })
      }
    }
    )
  }


  private lat;
  private long;
  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: Position) => {
        if (position) {
          console.log("Latitude: " + position.coords.latitude +
            "Longitude: " + position.coords.longitude);
          this.lat = position.coords.latitude;
          this.long = position.coords.longitude;
          for (let i = 0;i < this.dataSource.filteredData.length;i++){
          this.dataSource.filteredData[i].distance=this.getDistanceFromLatLonInKm(this.lat,this.long,this.dataSource.filteredData[i].latitude ,this.dataSource.filteredData[i].longitude);
          }
         console.log( this.dataSource.filteredData.length);
         console.log( this.dataSource.filteredData[1].latitude);

        }
      },
        (error: PositionError) => console.log(error));
    } else {
      alert("Geolocation is not supported by this browser.");
    }
    return true;
  }


  getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    
    return +d.toFixed(2);
    
  }

  isTracking;
  
}
 

function deg2rad(deg) {
  return deg * (Math.PI/180)
}