package com.gfi.parking.parking.controllers;


import com.gfi.parking.parking.DTO.AdresseDTO;
import com.gfi.parking.parking.service.AdresseService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@RequestMapping("/v1/adresses")
@CrossOrigin(origins = "*")
public class AdresseController {

    private final AdresseService adresseService;
    public AdresseController(AdresseService adresseService) {
        this.adresseService = adresseService;
    }

    @GetMapping("/")
    public ResponseEntity findAll(){
        return ResponseEntity.ok(adresseService.findAll());
    }

    @GetMapping("/{idAdr}")
    public  ResponseEntity findAdresseById(@PathVariable(name="idAdr") Long idAdr){
        if (idAdr==null)
            return ResponseEntity.badRequest().body("idAdr not specified");

        Optional<AdresseDTO> adresse = adresseService.findOne(idAdr);
        if (!adresse.isPresent()){
            System.out.println("adresse = null");
            return ResponseEntity.notFound().build();
        }
            return  ResponseEntity.ok().body(adresse);

    }

    @PostMapping("/")
    public ResponseEntity createAdresse (@RequestBody AdresseDTO adresse){
       if (adresse== null)
           return ResponseEntity.badRequest().body("Empty Request Body");
        AdresseDTO createAdresse = adresseService.save(adresse);

        return ResponseEntity.ok(createAdresse);
    }

    @DeleteMapping("/{idAdr}")
    public ResponseEntity deleteAdessse(@PathVariable Long idAdr){

       if (idAdr==null)
           return ResponseEntity.badRequest().body("Empty idAdr field");

       Optional<AdresseDTO> adresse = adresseService.findOne(idAdr);

       if (!adresse.isPresent()){
           return ResponseEntity.notFound().build();
       }
        adresseService.delete(idAdr);

       return ResponseEntity.ok().body(adresse);
    }




}
