package com.gfi.parking.parking.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
@Data
public class Reservation implements Serializable {

    @Id
    @GeneratedValue
    private Long idReservation;
    private Long entre;
    private Long sortie;
    private Long idPark;
   /* @ManyToOne
    private  Personne personne;*/


}
