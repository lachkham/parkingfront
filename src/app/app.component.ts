import { Component, OnInit } from '@angular/core';
import { LoginService } from './service/personne/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'ParkingFront';
  constructor(private loginService:LoginService){};
  ngOnInit(): void {
    this.loginService.checkSession().subscribe(
      res =>   {
        console.log("Logged In");
      },
      error => {
        localStorage.clear();        
      }
    )
  }
}
