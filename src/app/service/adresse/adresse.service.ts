import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppSettings } from 'src/app/settings/app.settings';
import { Adresse } from 'src/app/models/adresseModel';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdresseService {
  headers ;

  constructor(private http : HttpClient) {if(localStorage.getItem('xAuthToken')!=null){
    this.headers = new HttpHeaders({'x-auth-token' : localStorage.getItem('xAuthToken')});
  }else{
    this.headers=null;
  } }


  findAll(): Observable<any>{
    return this.http.get(AppSettings.App_URL+"/adresses/", {headers: this.headers});
  }
  getOne(id): Observable<Adresse>{
    return this.http.get<Adresse>(AppSettings.App_URL+"/adresses/"+id, {headers: this.headers});
  }
  AddOne(adresse : Adresse): Observable<Adresse>{
    return this.http.post<Adresse>(AppSettings.App_URL+"/adresses/",adresse, {headers: this.headers});
  }
  DeleteOne(id){
    return this.http.delete(AppSettings.App_URL+"/adresses/"+id, {headers: this.headers});
  }
}
 