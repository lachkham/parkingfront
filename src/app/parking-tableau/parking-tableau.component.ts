import { AfterViewInit, Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { ParkingService } from '../service/parking/parking.service';
import { Parking } from '../models/parkingModel';
import { MapsComponentComponent } from '../maps-component/maps-component.component';
import { ReservationService } from '../service/parking/reservation/reservation.service';

@Component({
  selector: 'app-parking-tableau',  
  templateUrl: './parking-tableau.component.html',
  styleUrls: ['./parking-tableau.component.css']
})
export class ParkingTableauComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Parking>; 
  @Output() CoordEvent = new EventEmitter<number[]>();

    sendCoordinate(latitude,longitude){

      this.CoordEvent.emit([latitude,longitude]);
    }


  

  dataSource = new MatTableDataSource([]);
  myDate = new Date();
  
  constructor(private parkingService : ParkingService,private reservationService:ReservationService){
  }

  displayedColumns = ['libelle','distance','nbPlace'];

  ngOnInit() {
  }
  parkings;
  ngAfterViewInit() {
    this.parkingService.findAll().subscribe(allitem =>{
      /*for (let i = 0;i < allitem.length;i++){
      allitem[i].distance=this.getDistanceFromLatLonInKm(this.lat,this.long,allitem[i].latitude ,allitem[i].longitude);
    }*/

      this.dataSource = new MatTableDataSource(allitem);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.table.dataSource = this.dataSource;
      
    },
    err=>{},
    ()=>{
      this.getLocation();
      for (let i = 0;i < this.dataSource.filteredData.length;i++){
          this.reservationService.nbReservation(this.dataSource.filteredData[i].idParking,this.myDate.getTime(),0).subscribe(nbplace=>{
            this.dataSource.filteredData[i].nbPlace=this.dataSource.filteredData[i].nbPlace-nbplace;
            console.log(this.dataSource.filteredData[i].idParking+" "+nbplace);
          })
      }
    }
    )
  }


  private lat;
  private long;
  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: Position) => {
        if (position) {
          console.log("Latitude: " + position.coords.latitude +
            "Longitude: " + position.coords.longitude);
          this.lat = position.coords.latitude;
          this.long = position.coords.longitude;
          for (let i = 0;i < this.dataSource.filteredData.length;i++){
          this.dataSource.filteredData[i].distance=this.getDistanceFromLatLonInKm(this.lat,this.long,this.dataSource.filteredData[i].latitude ,this.dataSource.filteredData[i].longitude);
          }
         console.log( this.dataSource.filteredData.length);
         console.log( this.dataSource.filteredData[1].latitude);

        }
      },
        (error: PositionError) => console.log(error));
    } else {
      alert("Geolocation is not supported by this browser.");
    }
    return true;
  }


  getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    
    return +d.toFixed(2);
    
  }

  isTracking;
  
}
 

function deg2rad(deg) {
  return deg * (Math.PI/180)
}