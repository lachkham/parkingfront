import { Component, Inject, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Adresse } from '../models/adresseModel';
import { ReservationService } from '../service/parking/reservation/reservation.service';
import { ParkingService } from '../service/parking/parking.service';
import { AdresseService } from '../service/adresse/adresse.service';
import { Reservation } from '../models/reservationModel';
import { MatSnackBar } from '@angular/material/snack-bar';

export interface DialogData {
  latitude;
   longitude;
   idParking;
   reservation:Reservation;
}




@Component({
  selector: 'app-add-reservation',
  templateUrl: './add-reservation.component.html',
  styleUrls: ['./add-reservation.component.css']
})
export class AddReservationComponent implements OnInit,AfterViewInit{
  
  addressForm = this.fb.group({
    dateTimeEntre: [null, Validators.required],
    dateTimeSortie: [null, Validators.required],
    parking: [null, Validators.required],
    Emailuser:new FormControl('', [
      Validators.required,
      Validators.email,
    ]),
  
  });

  hasUnitNumber = false;

 
  parkings=[];

  constructor(private fb: FormBuilder,
    public dialogRef: MatDialogRef<AddReservationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private reservationService: ReservationService,
    private parkinService: ParkingService,
    private adressService:AdresseService,
    private _snackBar: MatSnackBar
    ) {}



  onSubmit() {
    let dte=new Date(this.addressForm.get('dateTimeEntre').value);
    let dts=new Date(this.addressForm.get('dateTimeSortie').value);
    let reservation = new Reservation();
    reservation.entre=dte.getTime();
    reservation.sortie=dts.getTime();
    reservation.idParking=this.addressForm.get('parking').value;
    reservation.email=this.addressForm.get('Emailuser').value;

    
    let nbPlace;
    
    if (reservation.entre!=null&& reservation.sortie!=null&&reservation.idParking!=null){
      console.log(reservation.idParking+" , "+reservation.entre+" , "+reservation.sortie);
      let mydate=new Date().getTime();

    this.reservationService.nbReservation(reservation.idParking,reservation.entre,reservation.sortie).subscribe(res=>{
          nbPlace=res;
          console.log(nbPlace);

    },
    err=>{},
    ()=>{
                this.parkinService.getOne(reservation.idParking).subscribe(parking=>{
                      nbPlace=parking.nbPlace-nbPlace;
                },err=>{},()=>{
                  if(nbPlace>0){
                    this.reservationService.AddOne(reservation).subscribe(res=>{
                      console.log(res);
                      this.dialogRef.close();
                      this.openSnackBar("Réservé!","fermer");

      
            })
                  }else{
                    this.openSnackBar("Le parking est complet,il n'y a pas de place","fermer");

                  }
                })
    })


     
    }
    }
    ngAfterViewInit(): void {
      //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
      //Add 'implements AfterViewInit' to the class.
      
    }
  ngOnInit() {
   
    if(this.data.reservation!=null){
      this.addressForm.get('dateTimeEntre').setValue(new Date(this.data.reservation.entre));  
      this.addressForm.get('dateTimeSortie').setValue(new Date(this.data.reservation.sortie));  
      this.addressForm.get('Emailuser').setValue(this.data.reservation.email);  

    }


   this.parkinService.findAll().subscribe(res=>{
     this.parkings=res;     
   },
   err=>{},
   ()=>{
    console.log(this.data);

     if(this.data.latitude!=null&&this.data.longitude!=null){
    for (let i = 0;i < this.parkings.length;i++){
      this.parkings[i].distance=this.getDistanceFromLatLonInKm(this.data.latitude,this.data.longitude,this.parkings[i].latitude ,this.parkings[i].longitude);
      }
      if(this.data.idParking!=null){
        this.addressForm.get('parking').setValue(this.data.idParking);
      }
    }
   });
    

  }

  public selectedMoments = [
    new Date(2018, 1, 12, 10, 30),
    new Date(2018, 3, 21, 20, 30)
];


getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  
  return +d.toFixed(2);
  
}
lat;
long;

openSnackBar(message: string, action: string) {
  this._snackBar.open(message, action, {
    duration: 2000,
  });
}

}
function deg2rad(deg) {
  return deg * (Math.PI/180)
}
