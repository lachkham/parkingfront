package com.gfi.parking.parking.service.impl;

import com.gfi.parking.parking.DTO.ParkingDTO;
import com.gfi.parking.parking.DTO.mapper.ParkingMapper;
import com.gfi.parking.parking.repository.ParkingRepository;
import com.gfi.parking.parking.service.ParkingService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
@Transactional

public class ParkingServiceImpl implements ParkingService {

    private  final ParkingMapper parkingMapper;
    private final ParkingRepository parkingRepository;


    public ParkingServiceImpl(ParkingMapper parkingMapper, ParkingRepository parkingRepository) {
        this.parkingMapper = parkingMapper;
        this.parkingRepository = parkingRepository;
    }


    @Override
    public ParkingDTO save(ParkingDTO parkingDTO) {
        return parkingMapper.toDto(parkingRepository.save(parkingMapper.toEntity(parkingDTO)));
    }

    @Override
    @Transactional(readOnly = true)
    public List<ParkingDTO> findAll() {
        return parkingMapper.toDto(parkingRepository.findAll());
    }

    @Override
    public Optional<ParkingDTO> findOne(Long id) {
        return Optional.of(parkingMapper.toDto(parkingRepository.findById(id).get()));
    }

    @Override
    public void delete(Long id) {
        parkingRepository.deleteById(id);

    }

    @Override
    public Optional<ParkingDTO> findByLongitudeAndLatitude(double longitude, double latitude) {
        //return Optional.of(parkingMapper.toDto(parkingRepository.findByLongitudeAndLatitude(longitude,latitude)));
        return Optional.of(parkingMapper.toDto(parkingRepository.findByLongitudeBetweenAndLatitudeBetween(longitude-0.000001,longitude+0.000001,latitude-0.000001,latitude+0.000001)));

    }
}
