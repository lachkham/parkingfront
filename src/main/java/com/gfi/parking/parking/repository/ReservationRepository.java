package com.gfi.parking.parking.repository;


import com.gfi.parking.parking.entities.Parking;
import com.gfi.parking.parking.entities.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation,Long> {

                List<Reservation> findByEntreAfterAndSortieBefore(Long entre,Long Sortie);
                Long countByIdParkAndEntreAfterAndSortieBefore(Long parking,Long entre, Long sortie);

                Long countByIdParkAndEntreAfter(Long parking,Long entre);


    @Query("SELECT count(r) FROM Reservation r WHERE (r.idPark=?1 and ((r.entre<?2 and ?2<r.sortie) or (r.entre<?3 and ?3<r.sortie ) or (?2<r.entre and r.sortie<?3)))")
    Long countParking(Long parking,Long entre, Long sortie);
}


