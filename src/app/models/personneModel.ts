export class Personne{
    cin         : string;
    prenom      : string;
    nom         : string;
    dateNaiss   : Date;
    username    : string
    password    : string;
    email       : string;
    idAdr       : number;
    status      : Boolean;
    superior    : String

}