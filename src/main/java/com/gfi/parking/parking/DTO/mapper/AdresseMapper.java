package com.gfi.parking.parking.DTO.mapper;

import com.gfi.parking.parking.DTO.AdresseDTO;
import com.gfi.parking.parking.entities.Adresse;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface AdresseMapper extends EntityMapper<AdresseDTO,Adresse>{



    default Adresse fromId( Long id) {
        if (id == null) {
            return null;
        }
         Adresse library=new Adresse();
        library.setIdAdr(id);
        return library;
    }
}
