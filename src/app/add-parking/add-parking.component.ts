import { Component, Inject, SimpleChanges } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Adresse } from '../models/adresseModel';
import { AdresseService } from '../service/adresse/adresse.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TypeParkingService } from '../service/parking/typeParking/type-parking.service';
import { ParkingService } from '../service/parking/parking.service';
import { Parking } from '../models/parkingModel';

@Component({
  selector: 'app-add-parking',
  templateUrl: './add-parking.component.html',
  styleUrls: ['./add-parking.component.css']
})
export class AddParkingComponent {
    date:String;
ngOnChanges(changes: SimpleChanges): void {
 
  this.date = new Date().toISOString().slice(0, 16);

}
  addressForm = this.fb.group({
    adresse: [null, Validators.required],
    Ville: [null, Validators.required],
    Gouvernorat: [null, Validators.required],
    Pays: [null, Validators.required],
    postalCode: [null, Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(5)])],
    latitude:[null, Validators.required],
    longitude:[null, Validators.required],
    libelle:[],
    nbPlace:[null, Validators.required],
    typeParking:[null, Validators.required],
    dateTime:[]
  });

  hasUnitNumber = false;

  states = [
    {name: 'Ariana', abbreviation: 'Ariana'},
    {name: 'Béja', abbreviation: 'Béja'},
    {name: 'Ben Arous', abbreviation: 'Ben Arous'},
    {name: 'Bizerte', abbreviation: 'Bizerte'},
    {name: 'Gabès', abbreviation: 'Gabès'},
    {name: 'Gafsa', abbreviation: 'Gafsa'},
    {name: 'Jendouba', abbreviation: 'Jendouba'},
    {name: 'Kairouan', abbreviation: 'Kairouan'},
    {name: 'Kasserine', abbreviation: 'Kasserine'},
    {name: 'Kebili', abbreviation: 'Kebili'},
    {name: 'Kef', abbreviation: 'Kef'},
    {name: 'Mahdia', abbreviation: 'Mahdia'},
    {name: 'Manouba', abbreviation: 'Manouba'},
    {name: 'Medenine', abbreviation: 'Medenine'},
    {name: 'Monastir', abbreviation: 'Monastir'},
    {name: 'Nabeul', abbreviation: 'Nabeul'},
    {name: 'Sfax', abbreviation: 'Sfax'},
    {name: 'Sidi Bouzid', abbreviation: 'Sidi Bouzid'},
    {name: 'Siliana', abbreviation: 'Siliana'},
    {name: 'Sousse', abbreviation: 'Sousse'},
    {name: 'Tataouine', abbreviation: 'Tataouine'},
    {name: 'Tozeur', abbreviation: 'Tozeur'},
    {name: 'Tunis', abbreviation: 'Tunis'},
    {name: 'Zaghouan', abbreviation: 'Zaghouan'}
  ];
  typeParkings=[];

  constructor(private fb: FormBuilder,
    private adresseService : AdresseService,
    private typeParkingService: TypeParkingService,
    private parkingService: ParkingService,
    public dialogRef: MatDialogRef<AddParkingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Parking) {
    }

  ngOnInit(): void {
    console.log(this.data);
    if (this.data!=null){
    this.addressForm.get('libelle').setValue(this.data.libelle);  
    this.addressForm.get('longitude').setValue(this.data.longitude);  
    this.addressForm.get('latitude').setValue(this.data.latitude);  
    this.addressForm.get('typeParking').setValue(this.data.typeId);  
    this.addressForm.get('nbPlace').setValue(this.data.nbPlace);
      
    this.adresseService.getOne(this.data.idAdr).subscribe(res =>{
      
      this.addressForm.get('adresse').setValue(res.adresse); 
      this.addressForm.get('Ville').setValue(res.ville); 
      this.addressForm.get('Gouvernorat').setValue(res.gouvernorat); 
      this.addressForm.get('Pays').setValue(res.pays); 
      this.addressForm.get('postalCode').setValue(res.codePostal); 
    })
  
  
  
  
  } 
    this.typeParkingService.findAll().subscribe(res =>{
        this.typeParkings=res;
       });
       
  }
  onSubmit() {

    
    
    let parking = new Parking();
    let adresse = new Adresse();
    
if (this.data!=null){
  parking.idParking=this.data.idParking;
  adresse.idAdr=this.data.idAdr;
}


    parking.libelle     =this.addressForm.get('libelle').value;
    parking.longitude   =this.addressForm.get('longitude').value
    parking.latitude    =this.addressForm.get('latitude').value;
    parking.typeId      =this.addressForm.get('typeParking').value;
    parking.nbPlace     =this.addressForm.get('nbPlace').value;
      

    adresse.adresse =this.addressForm.get("adresse").value;
    adresse.ville =this.addressForm.get("Ville").value;
    adresse.gouvernorat =this.addressForm.get("Gouvernorat").value;
    adresse.pays =this.addressForm.get("Pays").value;
    adresse.codePostal =this.addressForm.get("postalCode").value;

if(parking.libelle!=null&&parking.longitude!=null&&parking.latitude!=null&&parking.typeId!=null&&parking.nbPlace!=null
   && adresse.adresse!=null&&adresse.ville!=null&&adresse.gouvernorat!=null&&adresse.pays!=null&&adresse.codePostal!=null){
    
    
    
    this.adresseService.AddOne(adresse).subscribe(
      data => {
        parking.idAdr     = data.idAdr;
      },
      err =>{},
      ()=>{

          this.parkingService.AddOne(parking).subscribe(data=>{
            console.log(data);
          },
          err=>{},
          ()=>{
            this.dialogRef.close(true);
          });


      }
    );


   } 
  }

  
  onNoClick(): void {
    this.dialogRef.close();
  }
}
