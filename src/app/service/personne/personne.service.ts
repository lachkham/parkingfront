import { Injectable } from '@angular/core';
import { AppSettings } from 'src/app/settings/app.settings';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Personne } from 'src/app/models/personneModel';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PersonneService {
  headers;

  constructor(private http : HttpClient) {
    if(localStorage.getItem('xAuthToken')!=null){
      this.headers = new HttpHeaders({'x-auth-token' : localStorage.getItem('xAuthToken')});
    }else{
      this.headers=null;
    }
   }


  findAll():Observable<Personne[]>{
    return this.http.get<Personne[]>(AppSettings.App_URL+"/personnes/", {headers: this.headers});
  }
  getCurrentUser():Observable<Personne>{
    return this.http.get<Personne>(AppSettings.App_URL+"/personnes/getCurrentUser/",{headers: this.headers})
  }
  getOne(id):Observable<Personne>{
    return this.http.get<Personne>(AppSettings.App_URL+"/personnes/"+id, {headers: this.headers});
  }
  AddOne(personne : Personne):Observable<Personne>{
    return this.http.post<Personne>(AppSettings.App_URL+"/personnes/",personne, {headers: this.headers});
  }
  DeleteOne(id){
    return this.http.delete(AppSettings.App_URL+"/personnes/"+id, {headers: this.headers});
  }
  EnableDisable(personne : Personne):Observable<Personne>{
    personne.status=!personne.status;
    return this.http.post<Personne>(AppSettings.App_URL+"/personnes/",personne, {headers: this.headers});
  }
 

}
